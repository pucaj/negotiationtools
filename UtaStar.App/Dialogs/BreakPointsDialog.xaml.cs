﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace UTAStar.App.Dialogs
{
    /// <summary>
    /// Logika interakcji dla klasy BreakPointsDialog.xaml
    /// </summary>
    public partial class BreakPointsDialog : Window
    {
        public Dictionary<string, string> CriteriaNumberOfBreakPoints { get; private set; } = new Dictionary<string, string>();

        public BreakPointsDialog(IEnumerable<string> options)
        {
            InitializeComponent();

            CreateControls(options);
        }

        private void CreateControls(IEnumerable<string> options)
        {
            foreach (var option in options)
            {
                var container = new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                    Margin = new Thickness(5)
                };

                var label = new Label
                {
                    Content = option,
                    Margin = new Thickness(0, 0, 5, 0)
                };

                var textBox = new TextBox();

                container.Children.Add(label);
                container.Children.Add(textBox);
                ControlsStackPanel.Children.Add(container);
            }
        }

        private void GetValues()
        {
            foreach (StackPanel stackPanel in ControlsStackPanel.Children)
            {
                var label = (Label)stackPanel.Children[0];
                var textBox = (TextBox)stackPanel.Children[1];

                CriteriaNumberOfBreakPoints.Add(label.Content.ToString(), textBox.Text.Trim());
            }
        }

        private bool ValidateInputs()
        {
            foreach (StackPanel stackPanel in ControlsStackPanel.Children)
            {
                var textBox = (TextBox)stackPanel.Children[1];

                if (!int.TryParse(textBox.Text.Trim(), out int result))
                {
                    return false;
                }
            }

            return true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!ValidateInputs())
            {
                MessageBox.Show("Values must be integers!", "Invalid values");
                return;
            }

            DialogResult = true;

            GetValues();
            Close();
        }
    }
}