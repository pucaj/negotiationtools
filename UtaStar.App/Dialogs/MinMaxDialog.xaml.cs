﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace UTAStar.App.Dialogs
{
    /// <summary>
    /// Logika interakcji dla klasy MinMaxDialog.xaml
    /// </summary>
    public partial class MinMaxDialog : Window
    {
        public Dictionary<string, string> CriteriaMinMax { get; private set; } = new Dictionary<string, string>();

        public MinMaxDialog(IEnumerable<string> options)
        {
            InitializeComponent();

            CreateControls(options);
        }

        private void CreateControls(IEnumerable<string> options)
        {
            foreach (var option in options)
            {
                var container = new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                    Margin = new Thickness(5)
                };

                var label = new Label
                {
                    Content = option,
                    Margin = new Thickness(0, 0, 5, 0)
                };

                var comboBox = new ComboBox();
                comboBox.Items.Add("min");
                comboBox.Items.Add("max");
                comboBox.SelectedIndex = 0;

                container.Children.Add(label);
                container.Children.Add(comboBox);
                ControlsStackPanel.Children.Add(container);
            }
        }

        private void GetValues()
        {
            foreach (StackPanel stackPanel in ControlsStackPanel.Children)
            {
                var label = (Label)stackPanel.Children[0];
                var comboBox = (ComboBox)stackPanel.Children[1];

                CriteriaMinMax.Add(label.Content.ToString(), comboBox.SelectedItem.ToString());
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;

            GetValues();
            Close();
        }
    }
}
