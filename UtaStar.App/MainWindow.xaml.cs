﻿using ExcelDataReader;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Text;
using System.Windows;
using UTAStar.App.Dialogs;
using UTAStar.Common.Alghoritm;
using UTAStar.Common.Extensions;
using UTAStar.Common.Models;

namespace UTAStar.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private InputData inputData;

        public MainWindow()
        {
            InitializeComponent();
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        private void OpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "Excel Worsheets|*.xls;*xlsx|All Files|*.*";
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Multiselect = false;

            var fileName = string.Empty;

            if (ofd.ShowDialog() ?? false)
            {
                fileName = ofd.FileName;
            }

            if (string.IsNullOrEmpty(fileName))
            {
                return;
            }

            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                var result = reader.AsDataSet();

                dataGrid.DataContext = result.Tables[0];
            }

            dataScope.IsEnabled = true;
            columnNamesScope.IsEnabled = true;
            rowNamesScope.IsEnabled = true;
            ranksScope.IsEnabled = true;

            noDataIndicator.Visibility = Visibility.Collapsed;
            dataGrid.Visibility = Visibility.Visible;
        }

        private void GetDataFromScope(object sender, RoutedEventArgs e)
        {
            try
            {
                var rowNames = GetRowNames();
                var columnNames = GetColumnNames();
                var ranks = GetRanks();
                var data = GetData();
                var criteriaMinMax = GetCriteriaMinMax(columnNames);
                var criteriaNumberOfBreakPoints = GetCriteriaNumberOfBreakPoints(columnNames);

                inputData = new InputData
                {
                    Epsilon = 0.01,
                    RowNames = rowNames,
                    ColumnNames = columnNames,
                    Data = data,
                    AlternativesRanks = ranks,
                    CriteriaMinMax = criteriaMinMax,
                    CriteriaNumberOfBreakPoints = criteriaNumberOfBreakPoints
                };

                processButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private List<bool> GetCriteriaMinMax(IEnumerable<string> options)
        {
            var minMaxDialog = new MinMaxDialog(options);

            if (minMaxDialog.ShowDialog() ?? false)
            {
                return minMaxDialog.CriteriaMinMax
                    .Select(x => x.Value == "max")
                    .ToList();
            }

            throw new Exception("Criteria min max are missing!");
        }

        private List<int> GetCriteriaNumberOfBreakPoints(IEnumerable<string> options)
        {
            var breakPointsDialog = new BreakPointsDialog(options);

            if (breakPointsDialog.ShowDialog() ?? false)
            {
                return breakPointsDialog.CriteriaNumberOfBreakPoints
                    .Select(x => int.Parse(x.Value))
                    .ToList();
            }

            throw new Exception("Criteria number of break points are missing!");
        }

        private SheetScope GetScopeFromText(string scope)
        {
            try
            {
                var rawRange = scope.Trim().Replace(" ", string.Empty);
                var ranges = rawRange.Split(';');

                var rowRange = ranges[0]
                    .Split('-')
                    .Select(x => int.Parse(x))
                    .ToList();

                var columnRange = ranges[1]
                    .Split('-')
                    .Select(x => int.Parse(x))
                    .ToList();

                return new SheetScope
                {
                    RowStart = rowRange.First(),
                    RowEnd = rowRange.Last(),
                    ColumnStart = columnRange.First(),
                    ColumnEnd = columnRange.Last()
                };
            }
            catch (Exception)
            {
                throw;
            }
        }

        private IEnumerable<IEnumerable<object>> GetRecordsFromSheet(SheetScope sheetScope)
        {
            var table = dataGrid.DataContext as DataTable;

            return table.GetValues(sheetScope);
        }

        private List<string> GetRowNames()
        {
            if (string.IsNullOrEmpty(rowNamesScope.Text))
            {
                return null;
            }

            var sheetScope = GetScopeFromText(rowNamesScope.Text);

            if (sheetScope.ColumnStart != sheetScope.ColumnEnd)
            {
                throw new Exception("Row names must be in one column.");
            }

            var records = GetRecordsFromSheet(sheetScope);

            return records
                .SelectMany(x => x.Select(y => y as string))
                .ToList();
        }

        private List<string> GetColumnNames()
        {
            var sheetScope = GetScopeFromText(columnNamesScope.Text);

            if (sheetScope.RowStart != sheetScope.RowEnd)
            {
                throw new Exception("Column names must be in one row.");
            }

            var records = GetRecordsFromSheet(sheetScope);

            return records
                .SelectMany(x => x.Select(y => y as string))
                .ToList();
        }

        private List<int> GetRanks()
        {
            var sheetScope = GetScopeFromText(ranksScope.Text);

            if (sheetScope.ColumnStart != sheetScope.ColumnEnd)
            {
                throw new Exception("Ranks must be in one column.");
            }

            var records = GetRecordsFromSheet(sheetScope);

            return records
                .SelectMany(x => x)
                .Select(y => int.Parse(y.ToString()))
                .ToList();
        }

        private List<List<object>> GetData()
        {
            var sheetScope = GetScopeFromText(dataScope.Text);

            var records = GetRecordsFromSheet(sheetScope);

            return records
                .Select(x => x
                    .Select(y => y is DBNull ? null : y)
                    .ToList())
                .ToList();
        }

        private void processButton_Click(object sender, RoutedEventArgs e)
        {
            if (InputData.IsNullOrUnset(inputData))
            {
                MessageBox.Show("Input data is not fully set.", "Error");
                return;
            }

            var algorithm = new Common.Alghoritm.UTAStar();

            try
            {
                algorithm.ClearEngine();
                algorithm.Initialize();
                algorithm.Calculate(inputData);

                ShowResult(algorithm);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void ShowResult(Common.Alghoritm.UTAStar alghoritm)
        {
            var names = alghoritm.InputData.RowNames.ToArray();
            var ranks = alghoritm.Ranks;
            var values = alghoritm.OverallValues;

            var sb = new StringBuilder();

            for (int i = 0; i < ranks.Count(); i++)
            {
                sb.AppendLine($"#{ranks[i]} {names[i]} value: {values[i]}");
            }

            MessageBox.Show(sb.ToString(), "Result");
        }
    }
}