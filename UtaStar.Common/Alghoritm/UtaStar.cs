﻿using RDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UTAStar.Common.Models;

namespace UTAStar.Common.Alghoritm
{
    public class UTAStar
    {
        public bool Calculated { get; private set; }
        public InputData InputData { get; private set; }
        public List<int> Ranks { get; set; }
        public List<double> OverallValues { get; set; }

        private REngine engine;
        private bool initialized;

        public UTAStar()
        {
            InitializeEngine();
        }

        private void InitializeEngine()
        {
            REngine.SetEnvironmentVariables();
            engine = REngine.GetInstance();
        }

        public void Initialize()
        {
            InstallLibraries();
            ImportLibraries();

            initialized = true;
        }

        private void InstallLibraries()
        {
            engine.Evaluate("if(\"remotes\" %in% rownames(installed.packages()) == FALSE) { install.packages(\"remotes\") }");
            engine.Evaluate("if(\"MCDA\" %in% rownames(installed.packages()) == FALSE) { remotes::install_github(\"paterijk/MCDA\") }");
        }

        private void ImportLibraries()
        {
            try
            {
                engine.Evaluate("library(MCDA)");
            }
            catch (Exception ex)
            {
                throw new Exception("Library is currently used. Please close other R applications and try again. If didn't work, please restart this program.", ex);
            }
        }

        public void ClearEngine()
        {
            engine.ClearGlobalEnvironment();
        }

        public bool Calculate(InputData inputData)
        {
            Calculated = false;

            if (!initialized)
            {
                throw new Exception("Algorithm has not been initialized!");
            }

            InputData = inputData;

            try
            {
                PrepareData(inputData);
                EvaluateUTAStar();
                RetrieveData();
            }
            catch (Exception)
            {
                throw;
            }

            Calculated = true;
            return true;
        }

        private void PrepareData(InputData inputData)
        {
            SetEpsilon(inputData.Epsilon);
            PrepareData(inputData.Data);
            NamingTheTable(inputData.RowNames, inputData.ColumnNames);
            SetAlternativeRanks(inputData.AlternativesRanks);
            SetCriteriaMinMax(inputData.CriteriaMinMax);
            SetCriteriaBreakPointsNumber(inputData.CriteriaNumberOfBreakPoints);
        }

        private void EvaluateUTAStar()
        {
            engine.Evaluate("x1 <- UTASTAR(table, criteriaMinMax, criteriaNumberOfBreakPoints, epsilon, alternativesRanks = alternativesRanks)");
            //engine.Evaluate("stopifnot(x1$Kendall == 1)");
        }

        private void RetrieveData()
        {
            var rawRanks = engine.Evaluate("x1$ranks").AsList();
            var ranks = rawRanks.Select(x => x.AsInteger().First());

            Ranks = ranks.ToList();

            var rawOverallValues = engine.Evaluate("x1$overallValues").AsList();
            var overallValues = rawOverallValues.Select(x => x.AsNumeric().First());

            OverallValues = overallValues.ToList();
        }

        private void SetEpsilon(double epsilon)
        {
            engine.Evaluate($"epsilon <- {epsilon.ToString().Replace(",", ".")}");
        }

        private void PrepareData(IEnumerable<IEnumerable<object>> data)
        {
            var sb = new StringBuilder("table <- rbind(");
            var rows = new List<string>();

            foreach (var row in data)
            {
                var rowSb = new StringBuilder();

                rowSb.Append("c(");
                rowSb.Append(string.Join(",", row.Select(x => x.ToString().Replace(",", "."))));
                rowSb.Append(")");

                rows.Add(rowSb.ToString());
            }

            sb.Append(string.Join(",", rows));
            sb.Append(")");

            engine.Evaluate(sb.ToString());
        }

        private void NamingTheTable(IEnumerable<string> rowNames, IEnumerable<string> columnNames)
        {
            var sb = new StringBuilder();

            if (rowNames != null && rowNames.Any())
            {
                sb.Append("rownames(table) <- c(");
                sb.Append(string.Join(",", rowNames.Select(x => $"\"{x}\"")));
                sb.Append(")");

                engine.Evaluate(sb.ToString());
            }

            sb.Clear();

            if (columnNames != null && columnNames.Any())
            {
                sb.Append("colnames(table) <- c(");
                sb.Append(string.Join(",", columnNames.Select(x => $"\"{x}\"")));
                sb.Append(")");

                engine.Evaluate(sb.ToString());
            }
        }

        private void SetAlternativeRanks(IEnumerable<int> alternativesRanks)
        {
            engine.Evaluate($"alternativesRanks <- c({string.Join(",", alternativesRanks)})");
            engine.Evaluate("names(alternativesRanks) <- row.names(table)");
        }

        private void SetCriteriaMinMax(IEnumerable<bool> criteriaMinMax)
        {
            var sb = new StringBuilder();

            sb.Append("criteriaMinMax <- c(");
            sb.Append(string.Join(",", criteriaMinMax.Select(x => x ? "\"min\"" : "\"max\"")));
            sb.Append(")");

            engine.Evaluate(sb.ToString());
            engine.Evaluate("names(criteriaMinMax) <- colnames(table)");
        }

        private void SetCriteriaBreakPointsNumber(IEnumerable<int> criteriaBreakPointsNumber)
        {
            engine.Evaluate($"criteriaNumberOfBreakPoints <- c({string.Join(",", criteriaBreakPointsNumber)})");
            engine.Evaluate("names(criteriaNumberOfBreakPoints) <- colnames(table)");
        }
    }
}