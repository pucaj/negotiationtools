﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UTAStar.Common.Models;

namespace UTAStar.Common.Extensions
{
    public static class DataGridExtensions
    {
        public static IEnumerable<IEnumerable<object>> GetValues(this DataTable table)
        {
            var rows = table.Rows
                .AsList<DataRow>()
                .ToList();

            return rows.Select(x => x.ItemArray);
        }

        public static IEnumerable<IEnumerable<object>> GetValues(this DataTable table, SheetScope scope, int offset = 1)
        {
            var rows = table.Rows
                .AsList<DataRow>()
                .Skip(scope.RowStart - offset)
                .Take(scope.RowEnd - scope.RowStart + offset)
                .ToList();

            return rows.Select(x =>
                x.ItemArray
                    .Skip(scope.ColumnStart - offset)
                    .Take(scope.ColumnEnd - scope.ColumnStart + offset));
        }

        public static List<T> AsList<T>(this DataRowCollection collection)
        {
            var list = new List<T>();
            foreach (var item in collection)
            {
                list.Add((T)item);
            }

            return list;
        }

        public static List<object> AsList(this DataRowCollection collection)
        {
            var list = new List<object>();
            foreach (var item in collection)
            {
                list.Add(item);
            }

            return list;
        }

        public static List<T> AsList<T>(this DataColumnCollection collection)
        {
            var list = new List<T>();
            foreach (var item in collection)
            {
                list.Add((T)item);
            }

            return list;
        }

        public static List<object> AsList(this DataColumnCollection collection)
        {
            var list = new List<object>();
            foreach (var item in collection)
            {
                list.Add(item);
            }

            return list;
        }
    }
}
