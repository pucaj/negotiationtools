﻿using System.Collections.Generic;

namespace UTAStar.Common.Models
{
    public class InputData
    {
        public double Epsilon { get; set; }
        public IEnumerable<string> RowNames { get; set; }
        public IEnumerable<string> ColumnNames { get; set; }
        public IEnumerable<IEnumerable<object>> Data { get; set; }
        public IEnumerable<int> AlternativesRanks { get; set; }
        public IEnumerable<bool> CriteriaMinMax { get; set; }
        public IEnumerable<int> CriteriaNumberOfBreakPoints { get; set; }

        public static bool IsNullOrUnset(InputData inputData)
        {
            if (inputData == null)
            {
                return false;
            }

            var isAnyValuesNull = inputData.ColumnNames == null
                || inputData.Data == null
                || inputData.AlternativesRanks == null
                || inputData.CriteriaMinMax == null
                || inputData.CriteriaNumberOfBreakPoints == null;

            return isAnyValuesNull;
        }
    }
}
