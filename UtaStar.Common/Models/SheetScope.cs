﻿namespace UTAStar.Common.Models
{
    public class SheetScope
    {
        public int RowStart { get; set; }
        public int RowEnd { get; set; }
        public int ColumnStart { get; set; }
        public int ColumnEnd { get; set; }
    }
}
